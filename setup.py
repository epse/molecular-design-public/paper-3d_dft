from setuptools import find_packages, setup

setup(
    name='cdft',
    version='1.0',
    packages=find_packages(),
    install_requires=[
        'feos==0.6.1',
        'pymatgen==2023.8.10',
        'numpy==1.26.3'
    ],
)
