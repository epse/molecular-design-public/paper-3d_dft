import numpy as np

from .LJ_external_potential import LJExternalPotential
from .LJ_Coulomb_fast_external_potential import LJCoulombFastExternalPotential
from .LJ_Coulomb_full_external_potential import LJCoulombFullExternalPotential
from .variables import cutoff_LJ, convert_to_short_name, pore_blocking_folder, \
                      pore_blocking_radius, pore_blocking_radius_per_A3

class ExternalPotential:

    def __init__(self, framework, adsorbates, interaction_type, 
                 pore_blocking_bool, pore_blocking_file,temperature):  

        self.framework = framework
        self.adsorbates = adsorbates
        self.interaction_type = interaction_type

        self.external_potential_obj = []
        self.potential_calculation = np.zeros([len(self.adsorbates.name),
                                               self.framework.grid.shape[0],
                                               self.framework.grid.shape[1],
                                               self.framework.grid.shape[2]])
        
        self.pore_blocking_bool = pore_blocking_bool
        self.pore_blocking_file = pore_blocking_file
        
        # For all of the adsorbates calculate the external_potential
        for i in range(self.potential_calculation.shape[0]):
            if self.interaction_type[i] == 'LJ_only':
                self.external_potential_obj.append(LJExternalPotential(self.framework,
                                                                    self.adsorbates,
                                                                    i))
                self.potential_calculation[i] = self.external_potential_obj[i].LJ_pot

            elif interaction_type[i] == 'LJ_Coulomb_fast_sampling':
                self.external_potential_obj.append(LJCoulombFastExternalPotential(self.framework,
                                                                    self.adsorbates,
                                                                    i,
                                                                    temperature))
                self.potential_calculation[i] = self.external_potential_obj[i].coulomb_new
            
            elif interaction_type[i] == 'LJ_Coulomb_full_sampling':
                self.external_potential_obj.append(LJCoulombFullExternalPotential(self.framework,
                                                                    self.adsorbates,
                                                                    i,
                                                                    temperature))
                self.potential_calculation[i] = self.external_potential_obj[i].energies
            
            # Add the tail correction
            self.potential_calculation[i] += self.get_tail_correction(i)

            # Consider the pores blocked
            if self.pore_blocking_bool:
                self.pore_blocking(i)
            
    
    def get_tail_correction(self, idx_adsorbate):
        tail_corr = 0              
        for i in range(self.framework.num_atoms):
                tail_corr += \
                    self.get_single_tail_correction(self.framework.epsilon[i], 
                        self.adsorbates.epsilon_pcsaft[idx_adsorbate], 
                        self.framework.sigma[i], 
                        self.adsorbates.sigma_pcsaft[idx_adsorbate])
        # A factor 2 because in RASPA it looks like they are double counting
        # all interactions for the tail correction. Not sure why though, but
        # I don't understand the tail correction well.
        return 2*self.adsorbates.m_pcsaft[idx_adsorbate]*tail_corr
    
    def get_single_tail_correction(self, eps_at1, eps_at2, sigma_at1, sigma_at2):
        eps = np.sqrt(eps_at1*eps_at2)
        sig = (sigma_at1 + sigma_at2)/2
        tail_correc = 2*np.pi/self.framework.volume_cell*4/3*eps*sig**3*(1/3*(sig/cutoff_LJ)**9\
                                                         - (sig/cutoff_LJ)**3)
        return tail_correc
    
    def pore_blocking(self, idx_adsorbate):
        self.pore_volume_blocked = 0
        self.lattice_inv = np.linalg.inv(self.framework.lattice)
        
        block_path = self.pore_blocking_file
        self.get_block_spheres(block_path)
        for i in range(self.framework.grid.shape[0]):
            for j in range(self.framework.grid.shape[1]):
                for k in range(self.framework.grid.shape[2]):
                    for l in range(self.block_spheres.shape[0]):
                        dist = (self.block_spheres[l, :3] - self.framework.grid[i, j, k])\
                                @self.lattice_inv
                        dist = (dist - np.rint(dist))@self.framework.lattice
                        dist = np.linalg.norm(dist)
                        if dist < self.block_spheres[l, 3]:
                            self.potential_calculation[idx_adsorbate, i, j, k] = np.inf
                      
    
    def get_block_spheres(self, block_path):
        f = open(block_path, 'r')
        num_pores = int(f.readline())
        self.block_spheres = np.zeros([num_pores, 4])
        for i in range(num_pores):
            l = f.readline()
            l = l.split()
            self.block_spheres[i, :3] += float(l[0])*self.framework.lattice[0]
            self.block_spheres[i, :3] += float(l[1])*self.framework.lattice[1]
            self.block_spheres[i, :3] += float(l[2])*self.framework.lattice[2]
            self.block_spheres[i, 3] = float(l[3])
            self.pore_volume_blocked += 4/3*np.pi*(float(l[3]))**3
        f.close()