import numpy as np

from feos.si import *
from feos.dft import Pore3D, Adsorption3D, DFTSolver, \
                     HelmholtzEnergyFunctional, State, Verbosity

from .framework import Framework
from .adsorbate import Adsorbate
from .external_potential import ExternalPotential
from .variables import beta_picard_1_def, max_iter_picard_1_def,\
                      beta_picard_2_def, tol_picard_2_def, \
                      mmax_anderson_mixing_def, tol_anderson_mixing_def, \
                      fmt_version_def, max_energy
                      

class cDFT:

    def __init__(self, cif_path, framework_ff, adsorbates_name, interaction_type, 
                 adsorbates_pcsaft, adsorbates_external_pot, pore_blocking_bool, 
                 pore_blocking_file, n_grid_def, n_grid, temperature):  

        self.framework = Framework(cif_path, framework_ff, n_grid_def, n_grid)
        self.adsorbates = Adsorbate(adsorbates_name, adsorbates_pcsaft,
                                     adsorbates_external_pot, interaction_type)
        self.external_pot = ExternalPotential(self.framework, self.adsorbates,
                                              interaction_type, 
                                              pore_blocking_bool, pore_blocking_file, temperature)
        
        self.pore = Pore3D(self.framework.system_size, 
                    [self.framework.grid_size[0], self.framework.grid_size[1], self.framework.grid_size[2]], 
                    self.framework.coordinates.T * ANGSTROM, np.zeros(0), 
                    np.zeros(0), self.framework.angles)
        self.functional = HelmholtzEnergyFunctional.pcsaft(
            self.adsorbates.pcsaft_params, fmt_version_def)
        
        self.define_solvers()

    def define_solvers(self):
        # Define the cDFT solver and its parameters
        self.solver1 = DFTSolver()\
            .picard_iteration(damping_coefficient=beta_picard_1_def, 
                            max_iter=max_iter_picard_1_def)\
            .picard_iteration(damping_coefficient=beta_picard_2_def, 
                            tol=tol_picard_2_def)\
            .anderson_mixing(mmax=mmax_anderson_mixing_def, 
                            tol=tol_anderson_mixing_def)
        self.solver2 = DFTSolver()\
                            .newton(tol=tol_anderson_mixing_def)
        
        self.solver3 = DFTSolver().picard_iteration(log=True, max_iter=2000)

        self.solver4 = DFTSolver()\
            .picard_iteration(damping_coefficient=0.005, 
                            max_iter=2000)\
            .picard_iteration(damping_coefficient=0.01, 
                            tol=tol_picard_2_def)\
            .anderson_mixing(mmax=mmax_anderson_mixing_def, 
                            tol=tol_anderson_mixing_def)
        
    def compute_loading_and_enthalpy(self, temperature, pressures, adsorbate_frac):
        # Computing the adsorption isotherms using extra interactions
        self.error_bool = 0
        self.error_message = ''
        assert np.sum(adsorbate_frac) == 1

        self.loading_absolute = np.nan*np.ones([len(pressures), len(self.adsorbates.name)])
        self.enthalpy_of_adsorption = np.nan*np.ones([len(pressures), len(self.adsorbates.name)])
        self.density = None

        for num_p, p in enumerate(pressures):
            self.bulk = State(eos=self.functional, temperature=temperature*KELVIN,
                            pressure=p*BAR, molefracs=adsorbate_frac)
            self.external_pot_grid = self.external_pot.potential_calculation/temperature
            self.external_pot_grid[self.external_pot_grid > max_energy] = max_energy
            self.new_pore = self.pore.initialize(self.bulk,
                    external_potential=self.external_pot_grid,
                    density=self.density)
            
            try:
                self.compute_at_pressure(self.solver1, num_p)
            except: 
                try:
                    self.compute_at_pressure(self.solver3, num_p)
                except:
                    try:
                        self.compute_at_pressure(self.solver4, num_p)
                    except:
                        raise Exception("Error cDFT: All solvers failed to find a solution for {self.adsorbent} ({temperature}, {pressure}, and {adsorbate_frac}")

    def compute_at_pressure(self, solver, num_p):
        # Optimization of the density and extraction of loading and enthalpy of
        # adsorption
        self.new_pore.solve(solver=solver, debug=False)
        
        # Different extraction if a pure component is studied or a mixture
        self.loading_absolute[num_p] = self.new_pore.moles/ MOL/self.framework.mass
        self.enthalpy_of_adsorption[num_p] \
                = -self.new_pore.enthalpy_of_adsorption*MOL/(KILO * JOULE)
        self.density = self.new_pore.density
        # if self.pure_component == 0:
        #     self.loading_absolute = self.new_pore.moles/MOL / self.mass
        #     self.heat_of_adsorption \
        #         = -self.new_pore.partial_molar_enthalpy_of_adsorption*MOL/(KILO * JOULE)
        # else:
        #     self.loading_absolute = [0, 0]
        #     self.heat_of_adsorption = [0, 0]
        #     self.loading_absolute[self.adsorbate_index] \
        #         = self.new_pore.total_moles/MOL / self.mass
        #     self.heat_of_adsorption[self.adsorbate_index] \
        #         = -self.new_pore.enthalpy_of_adsorption*MOL/(KILO * JOULE)
