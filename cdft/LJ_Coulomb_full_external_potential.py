import itertools
import numpy as np

from .variables import cutoff_LJ, cutoff_coul, factor_coulomb, angle_step

class LJCoulombFullExternalPotential:

    def __init__(self, framework, adsorbates, idx_adsorbate, temperature):  

        self.framework = framework
        self.adsorbates = adsorbates
        self.idx_adsorbate = idx_adsorbate
        self.temperature = temperature

        self.periodic_epsilon_mix_LJ = np.sqrt(self.framework.periodic_epsilon_LJ[:, None]\
                                     * self.adsorbates.epsilon_ff[idx_adsorbate][None, :])
        self.periodic_sigma_mix_LJ = (self.framework.periodic_sigma_LJ[:, None] \
                                    + self.adsorbates.sigma_ff[idx_adsorbate][None, :])/2

        self.energies = self.get_energy_all_grid_points()

    def get_energy_all_grid_points(self):

        theta_angles = np.arange(angle_step, 360, angle_step)
        phi_angles = np.arange(0, 180, angle_step)
        num_config_angles = theta_angles.shape[0]*phi_angles.shape[0] + 1
        grid_1d = self.framework.grid.reshape((-1, 3))
        all_angles = list(itertools.product(theta_angles, phi_angles))
        all_angles.append((0, 0))
        all_info = list(itertools.product(all_angles, grid_1d))
        all_config = list(map(self.get_compute_pos_angles, all_info))
        energies = np.array(list(map(self.get_energy_single_molecule, all_config)))
        energies = energies.reshape((num_config_angles, self.framework.grid.shape[0], 
                                     self.framework.grid.shape[1], self.framework.grid.shape[2]))

        energies = np.sum(energies*np.exp(-energies/self.temperature)\
                            /(np.sum(np.exp(-energies/self.temperature), 
                                    axis=0)), axis=0)
        return energies
    
    def get_energy_single_molecule(self, pos_gas_atoms):
        # For each atom in the gas molecule, add the interaction with each 
        # of the atom in the MOF that is below the radius. 
        
        # # Compute the Lennard-Jones interactions

        distances_new = (self.framework.periodic_coord_LJ[:, None, :] \
                        - pos_gas_atoms[None, :, :])@self.framework.rep_lat_inv_LJ
        distances_new = (distances_new - np.rint(distances_new))@self.framework.rep_lat_LJ 
        distances_new = np.linalg.norm(distances_new, axis = -1)

        below = np.where(distances_new < cutoff_LJ)
        all_eps = self.periodic_epsilon_mix_LJ[below]
        all_sig = self.periodic_sigma_mix_LJ[below]
        all_dis = distances_new[below]
        all_LJ = 4*all_eps*((all_sig/all_dis)**12 - (all_sig/all_dis)**6) 
        LJ_pot = np.sum(all_LJ)

        # Compute the Coulomb interactions
        distances_coul = (self.framework.periodic_coord_coul[:, None, :] \
                         - pos_gas_atoms[None, :, :])@self.framework.rep_lat_inv_coul
        distances_coul = (distances_coul - np.rint(distances_coul))@self.framework.rep_lat_coul
        distances_coul = np.linalg.norm(distances_coul, axis=-1)


        below_coul_threshold = np.where(distances_coul[:, 0] < cutoff_coul)
        charge_prod_new = self.framework.periodic_charges_coul[:, None]*self.adsorbates.charges[self.idx_adsorbate][None, :]
        all_coul_new = charge_prod_new[below_coul_threshold]/distances_coul[below_coul_threshold]
        coul = np.sum(all_coul_new)
        coul *= factor_coulomb
        tot = coul + LJ_pot
        return tot
    
    def get_compute_pos_angles(self, all_info):
        grid_point_pos = all_info[1]
        new_pos_gas = np.zeros([self.adsorbates.coords[self.idx_adsorbate].shape[0], 3])
        theta = all_info[0][0]
        phi = all_info[0][1]
        theta_rad = theta/360*(2*np.pi)
        phi_rad = phi/360*(2*np.pi)
        for i in range(self.adsorbates.coords[self.idx_adsorbate].shape[0]):
            r = self.adsorbates.spherical_coords[self.idx_adsorbate][i,0]
            theta_gas = self.adsorbates.spherical_coords[self.idx_adsorbate][i, 1]
            phi_gas = self.adsorbates.spherical_coords[self.idx_adsorbate][i, 2]
            new_pos_gas[i, 0] = r*np.sin(theta_gas + theta_rad)*np.cos(phi_gas + phi_rad) + grid_point_pos[0]
            new_pos_gas[i, 1] = r*np.sin(theta_gas + theta_rad)*np.sin(phi_gas + phi_rad) + grid_point_pos[1]
            new_pos_gas[i, 2] = r*np.cos(theta_gas + theta_rad) + grid_point_pos[2]
        return new_pos_gas
    