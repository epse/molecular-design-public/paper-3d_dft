import numpy as np

from .variables import cutoff_LJ, cutoff_coul, factor_coulomb

class LJCoulombFastExternalPotential:

    def __init__(self, framework, adsorbates, idx_adsorbate, temperature):  

        self.framework = framework
        self.adsorbates = adsorbates
        self.idx_adsorbate = idx_adsorbate
        self.temperature = temperature

        self.periodic_epsilon_mix_LJ = np.sqrt(self.framework.periodic_epsilon_LJ[:, None]\
                                     * self.adsorbates.epsilon_ff[idx_adsorbate][None, :])
        self.periodic_sigma_mix_LJ = (self.framework.periodic_sigma_LJ[:, None] \
                                    + self.adsorbates.sigma_ff[idx_adsorbate][None, :])/2
        self.gas_coord = np.array([[0, 0, 0],
                                   [0, 0, self.adsorbates.length_mol[idx_adsorbate]],
                                   [0, 0, -self.adsorbates.length_mol[idx_adsorbate]]])
        self.GCMC_energy_clever_dir()


    def GCMC_energy_clever_dir(self):
        self.coulomb_new = np.zeros(self.framework.grid.shape[:3])
        for i in range(self.framework.grid_size[0]):
            for j in range(self.framework.grid_size[1]):
                for k in range(self.framework.grid_size[2]):
                    # Calculations of the minimal image convention
                    # for LJ interactions
                    self.positions_LJ = \
                        (self.framework.periodic_coord_LJ - self.framework.grid[i, j, k])\
                        @self.framework.rep_lat_inv_LJ
                    self.positions_LJ = \
                        (self.positions_LJ - np.rint(self.positions_LJ))\
                        @self.framework.rep_lat_LJ
                    self.distances_LJ = np.linalg.norm(self.positions_LJ, axis=1)
                    below_LJ_threshold = np.where(self.distances_LJ < cutoff_LJ)

                    # Calculations of the minimal image convetion for Coulombic
                    # interactions
                    self.positions_coul = \
                        (self.framework.periodic_coord_coul - self.framework.grid[i, j, k])\
                        @self.framework.rep_lat_inv_coul
                    self.positions_coul = (\
                        self.positions_coul - np.rint(self.positions_coul))\
                                @self.framework.rep_lat_coul
                    self.distances_coul = \
                        np.linalg.norm(self.positions_coul, axis=-1)
                    below_threshold = \
                            np.where(self.distances_coul < cutoff_coul)[0]
                    # If there is an atom closer than 2 Angstrom from the grid
                    # point, there will be very high interactions and computing
                    # the energy is not necessary.
                    if np.min(self.distances_coul) > 2:
                        self.coulomb_new[i, j, k] = self.get_single_GCMC_energy_clever_dir(
                            self.positions_coul[below_threshold],
                            self.framework.periodic_charges_coul[below_threshold],
                            self.positions_LJ[below_LJ_threshold],
                            self.periodic_epsilon_mix_LJ[below_LJ_threshold],
                            self.periodic_sigma_mix_LJ[below_LJ_threshold])
                    else:
                        self.coulomb_new[i, j, k] = np.inf 

    def get_single_GCMC_energy_clever_dir(self, pos_at, q_at, pos_LJ, eps, sig):
        self.eps = eps
        self.sig = sig
        self.pos_LJ = pos_LJ
        self.pos_at = pos_at
        self.q_at = q_at

        forbid_dir_LJ = self.get_forbid_dir_LJ(pos_LJ, eps, sig)

        best_dir_coul = self.get_best_dir_coul(pos_at, q_at)

        ang_1 = 180/np.pi*np.arccos(np.dot(forbid_dir_LJ, best_dir_coul) \
            / (np.linalg.norm(forbid_dir_LJ)*np.linalg.norm(best_dir_coul)))
        ang_2 = 180/np.pi*np.arccos(np.dot(forbid_dir_LJ, -best_dir_coul) \
            / (np.linalg.norm(forbid_dir_LJ)*np.linalg.norm(best_dir_coul)))
        if ang_1 < ang_2:
            coeff = 1
        else:
            coeff = -1
        
        diff_frac = np.arange(0, 1.1, 0.1)[None, :]*coeff*best_dir_coul[:, None] \
            + (1 - np.arange(0, 1.1, 0.1))[None, :]*forbid_dir_LJ[:, None]
        diff_frac = list(diff_frac.T)
        pos_from_dir = list(map(self.compute_pos_from_dir, diff_frac))
        coul_estimate = np.array(list(map(self.coulomb_inter, pos_from_dir)))
        LJ_estimate = np.array(list(map(self.LJ_inter, pos_from_dir)))
        tot_estimate = coul_estimate + LJ_estimate
        energy_min = np.min(tot_estimate)
        return energy_min
    
    def compute_pos_from_dir(self, direction):
        new_pos_gas = np.zeros([3, 3])
        new_pos_gas[0] = self.gas_coord[0]
        new_pos_gas[1, 0] = self.gas_coord[0, 0] \
                        + self.adsorbates.length_mol[self.idx_adsorbate]*direction[0]
        new_pos_gas[1, 1] = self.gas_coord[0, 1] \
                        + self.adsorbates.length_mol[self.idx_adsorbate]*direction[1]
        new_pos_gas[1, 2] = self.gas_coord[0, 2] \
                        + self.adsorbates.length_mol[self.idx_adsorbate]*direction[2]
        new_pos_gas[2, 0] = self.gas_coord[0, 0] \
                        - self.adsorbates.length_mol[self.idx_adsorbate]*direction[0]
        new_pos_gas[2, 1] = self.gas_coord[0, 1] \
                        - self.adsorbates.length_mol[self.idx_adsorbate]*direction[1]
        new_pos_gas[2, 2] = self.gas_coord[0, 2] \
                        - self.adsorbates.length_mol[self.idx_adsorbate]*direction[2]
        return new_pos_gas

    def get_forbid_dir_LJ(self, pos_LJ, eps, sig):
        # This function calculates the direction forbidden by the LJ interactions.
        # This direction is taken as the cross product of the two orientations
        # towards an atom of the adsorbent that gives the highest energy.
        dis_all_LJ = np.linalg.norm(pos_LJ, axis=1)
        directions_at_LJ = (pos_LJ.T/dis_all_LJ).T


        all_LJ = 4*eps[:, 0]*((sig[:, 0]/dis_all_LJ)**12 - (sig[:, 0]/dis_all_LJ)**6) \
        + 4*eps[:, 1]*((sig[:, 1]/(dis_all_LJ + self.adsorbates.length_mol[self.idx_adsorbate]))**12 - (sig[:, 1]/(dis_all_LJ + self.adsorbates.length_mol[self.idx_adsorbate]))**6) \
        + 4*eps[:, 2]*((sig[:, 2]/(dis_all_LJ - self.adsorbates.length_mol[self.idx_adsorbate]))**12 - (sig[:, 2]/(dis_all_LJ - self.adsorbates.length_mol[self.idx_adsorbate]))**6) 

        if len(all_LJ) == 0:
            # If no LJ interactions are less than the cutoff, then a random
            # direction is taken as the forbidde direction will be decided by
            # Coulombic interactions that have a larger cutoff.
            forbid_dir = np.array([1, 0, 0])
        else:
            # If there are LJ interactions, then the two largest LJ interactions
            # are used to define a direction that is forbidden. 
            idx_sorted_inter = np.argsort(all_LJ)[::-1]
            dir_1 = directions_at_LJ[idx_sorted_inter[0]]
            dir_1 = dir_1/np.linalg.norm(dir_1)
            if len(all_LJ) == 1:
                # If there is only one atom closer than the tradeoff then it must
                # be quite far, so the orientation will mostly be defined with 
                # the coulombic interactions. 
                dir_2 = np.array([1, 0, 0])
            else:
                dir_2 = directions_at_LJ[idx_sorted_inter[1]]
                dir_2 = dir_2/np.linalg.norm(dir_2)
            cross_prod = np.cross(dir_1, dir_2)
            forbid_dir =  cross_prod/np.linalg.norm(cross_prod)
        
        return forbid_dir
    
    def get_best_dir_coul(self, pos_at, q_at):
        # This gives the most favorable direction for the gas molecule given the
        # Coulombic interactions. Here, 
        dir_pos_charges_at = pos_at[np.where(q_at >= 0)]
        dis_pos_charges_at = np.linalg.norm(dir_pos_charges_at, axis=1)
        dir_neg_charges_at = pos_at[np.where(q_at < 0)]
        dis_neg_charges_at = np.linalg.norm(dir_neg_charges_at, axis=1)
        q_pos_charges_at = q_at[np.where(q_at >= 0)]
        q_neg_charges_at = q_at[np.where(q_at < 0)]

        pos_charge_interactions = factor_coulomb*q_pos_charges_at*(self.adsorbates.charges[self.idx_adsorbate][1]/(dis_pos_charges_at - self.adsorbates.length_mol[self.idx_adsorbate]) \
                                                + self.adsorbates.charges[self.idx_adsorbate][2]/(dis_pos_charges_at + self.adsorbates.length_mol[self.idx_adsorbate]) \
                                                + self.adsorbates.charges[self.idx_adsorbate][0]/(dis_pos_charges_at))
        neg_charge_interactions = factor_coulomb*q_neg_charges_at*(self.adsorbates.charges[self.idx_adsorbate][1]/(dis_neg_charges_at - self.adsorbates.length_mol[self.idx_adsorbate]) \
                                                + self.adsorbates.charges[self.idx_adsorbate][2]/(dis_neg_charges_at + self.adsorbates.length_mol[self.idx_adsorbate]) \
                                                + self.adsorbates.charges[self.idx_adsorbate][0]/(dis_neg_charges_at))
    
        neg_weights = np.exp(-pos_charge_interactions/self.temperature)/np.sum(np.exp(-pos_charge_interactions/self.temperature))
        if np.isnan(neg_weights).any():
            if np.where(neg_weights == 0)[0].shape[0] == neg_weights.shape[0] - 1:
                neg_weights[np.where(np.isnan(neg_weights))] = 1
        pos_weights = np.exp(neg_charge_interactions/self.temperature)/np.sum(np.exp(neg_charge_interactions/self.temperature))
        if np.isnan(pos_weights).any():
            nan_idx = np.where(np.isnan(pos_weights))[0]
            pos_weights[nan_idx] = 1/nan_idx.shape[0]            

        mean_dir_pos_charge_interactions = self.weighted_first_pca(dir_pos_charges_at, neg_weights)
        mean_dir_neg_charge_interactions = self.weighted_first_pca(dir_neg_charges_at, pos_weights)

        frac_pos_charge_interactions = np.sum(np.exp(-pos_charge_interactions/self.temperature))/(np.sum(np.exp(-pos_charge_interactions/self.temperature)) + np.sum(np.exp(neg_charge_interactions/self.temperature)))
        frac_neg_charge_interactions = np.sum(np.exp(neg_charge_interactions/self.temperature))/(np.sum(np.exp(-pos_charge_interactions/self.temperature)) + np.sum(np.exp(neg_charge_interactions/self.temperature)))
        
        final_dir = frac_pos_charge_interactions*mean_dir_pos_charge_interactions \
                    + frac_neg_charge_interactions*self.proj_vector_on_plane(mean_dir_pos_charge_interactions, mean_dir_neg_charge_interactions)
        final_dir = final_dir/np.linalg.norm(final_dir)

        return final_dir
    
    def weighted_first_pca(self, x, weights):
        if x.shape[0] > 1:
            x = x*np.sqrt(weights[:, None])
            eigenvalues, eigenvectors = np.linalg.eig(x.T@x)
            vec_mean = eigenvectors[:, np.argmax(eigenvalues)]
        else:
            vec_mean = x[0]
        vec_mean = vec_mean/np.linalg.norm(vec_mean)
        return vec_mean
    
    def proj_vector_on_plane(self, u, n):
    # finding norm of the vector n 
        n_norm = np.sqrt(sum(n**2))    

        # Apply the formula as mentioned above
        # for projecting a vector onto the orthogonal vector n
        # find dot product using np.dot()
        proj_of_u_on_n = (np.dot(u, n)/n_norm**2)*n

        # subtract proj_of_u_on_n from u: 
        # this is the projection of u on Plane P
        proj =  u - proj_of_u_on_n
        return proj
    
    def coulomb_inter(self, pos_gas):
        dis = np.linalg.norm(pos_gas[:, None] - self.pos_at[None, :], axis=-1)
        prod_charg = self.adsorbates.charges[self.idx_adsorbate][:, None]*self.q_at[None, :]
        coulomb = np.sum(prod_charg/dis)
        return coulomb*factor_coulomb
    
    def LJ_inter(self, pos_gas):
        dis = np.linalg.norm(pos_gas[None, :]- self.pos_LJ[:, None], axis=-1)
        LJ_interactions = 4*self.eps*((self.sig/dis)**12 - (self.sig/dis)**6)
        return np.sum(LJ_interactions)