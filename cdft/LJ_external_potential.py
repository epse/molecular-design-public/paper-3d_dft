import numpy as np

from .variables import cutoff_LJ

class LJExternalPotential:

    def __init__(self, framework, adsorbates, idx_adsorbate):  

        self.framework = framework
        self.adsorbates = adsorbates
        self.idx_adsorbate = idx_adsorbate

        self.periodic_epsilon_mix_LJ = np.sqrt(self.framework.periodic_epsilon_LJ\
                                     * self.adsorbates.epsilon_pcsaft[idx_adsorbate])
        self.periodic_sigma_mix_LJ = (self.framework.periodic_sigma_LJ \
                                    + self.adsorbates.sigma_pcsaft[idx_adsorbate])/2

        self.LJ_pot = np.zeros(self.framework.grid.shape[:3])
        for i in range(self.framework.grid_size[0]):
            for j in range(self.framework.grid_size[1]):
                for k in range(self.framework.grid_size[2]):
                    self.distances = \
                        (self.framework.periodic_coord_LJ - self.framework.grid[i, j, k])\
                        @self.framework.rep_lat_inv_LJ
                    self.distances = (self.distances - np.rint(self.distances)) \
                                @self.framework.rep_lat_LJ
                    self.distances = np.linalg.norm(self.distances, axis=-1)
                    below_threshold = \
                            np.where(self.distances < cutoff_LJ)[0]

                    all_eps = self.periodic_epsilon_mix_LJ[below_threshold]
                    all_sig = self.periodic_sigma_mix_LJ[below_threshold]
                    all_dis = self.distances[below_threshold]
                    all_LJ = self.adsorbates.m_pcsaft[idx_adsorbate]*4*all_eps \
                        *((all_sig/all_dis)**12 - (all_sig/all_dis)**6)
                    self.LJ_pot[i, j, k] = np.sum(all_LJ)