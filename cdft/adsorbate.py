import numpy as np
import pandas as pd

from feos.pcsaft import PcSaftParameters

from .variables import adsorbate_atoms, adsorbate_charges, adsorbate_coords, \
                        length_mol

class Adsorbate:

    def __init__(self, adsorbates_name, adsorbates_pcsaft,
                adsorbates_external_pot, interaction_type):  

        # Define PC-SAFT parameters
        assert len(adsorbates_name) == len(adsorbates_pcsaft)
        assert len(adsorbates_name) == len(adsorbates_external_pot)

        self.name = adsorbates_name
        self.tuple_params = [tuple([[self.name[i]], adsorbates_pcsaft[i]])\
                              for i in range(len(adsorbates_name))]
        self.pcsaft_params = PcSaftParameters.from_multiple_json(
                    self.tuple_params)
        self.m_pcsaft = [self.pcsaft_params.pure_records[i].model_record.m \
                  for i in range(len(adsorbates_name))]
        self.epsilon_pcsaft = [self.pcsaft_params.pure_records[i].model_record.epsilon_k \
                  for i in range(len(adsorbates_name))]
        self.sigma_pcsaft = [self.pcsaft_params.pure_records[i].model_record.sigma \
                  for i in range(len(adsorbates_name))]
        
        # Define parameters for forcefield calculations
        self.atoms = []
        self.charges = []
        self.length_mol = []
        self.coords = []
        self.num_atoms = []
        self.spherical_coords = []
        self.epsilon_ff = []
        self.sigma_ff = []
        for num_ads, ads in enumerate(self.name):
            if interaction_type[num_ads] != 'LJ_only':
                self.atoms.append(adsorbate_atoms[ads])
                self.charges.append(adsorbate_charges[ads])
                self.length_mol.append(length_mol[ads])
                self.coords.append(adsorbate_coords[ads])
                self.num_atoms.append(len(adsorbate_atoms[ads]))
                self.spherical_coords.append(self.get_spherical_coord(adsorbate_coords[ads]))
                epsilons, sigmas = self.get_LJ_params_gas(adsorbates_external_pot[num_ads],
                                                        num_ads)
                self.epsilon_ff.append(epsilons)
                self.sigma_ff.append(sigmas)
            else:
                self.atoms.append(np.nan)
                self.charges.append(np.nan)
                self.length_mol.append(np.nan)
                self.coords.append(np.nan)
                self.num_atoms.append(np.nan)
                self.spherical_coords.append(np.nan)
                self.epsilon_ff.append(np.nan)
                self.sigma_ff.append(np.nan)

    def get_spherical_coord(self, adsorbate_coord):
        adsorbate_spherical_coord = np.zeros([adsorbate_coord.shape[0], 3])
        for i in range(adsorbate_coord.shape[0]):
            x = adsorbate_coord[i, 0]
            y = adsorbate_coord[i, 1]
            z = adsorbate_coord[i, 2]
            r = np.sqrt(np.sum(adsorbate_coord[i]**2))
            if r == 0:
                adsorbate_spherical_coord[i] = [0,0,0]
            else:
                adsorbate_spherical_coord[i, 0] = r
                adsorbate_spherical_coord[i, 1] = np.arccos(z/r)
                if np.sqrt(x**2 + y**2) == 0:
                    adsorbate_spherical_coord[i, 2] =0
                else:
                    adsorbate_spherical_coord[i, 2] = \
                        np.sign(y)*np.arccos(x/np.sqrt(x**2 + y**2))
        return adsorbate_spherical_coord
    
    def get_LJ_params_gas(self, adsorbate_ff, num_adsorbate):

        # Read force field file
        forcefield = pd.read_csv(adsorbate_ff, sep=r'\s+',  
                                  names=["type","sigma","epsilon","mass"])

        epsilons = np.zeros([self.num_atoms[num_adsorbate]])
        sigmas = np.zeros([self.num_atoms[num_adsorbate]])

        # For each atom, define the Lennard Jones parameters (sigma and epsilon)
        # in function of the atom type. Compute the total mass of the solid.
        for i in range(self.num_atoms[num_adsorbate]):
            atom_idx = forcefield["type"]==self.atoms[num_adsorbate][i]
            epsilons[i] = float(forcefield["epsilon"][atom_idx].iloc[0])
            sigmas[i] = float(forcefield["sigma"][atom_idx].iloc[0])
        
        return epsilons, sigmas