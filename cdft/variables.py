import numpy as np
from feos.dft import FMTVersion

cifparser_occupancy_tolerance_def = 100
mass_atom = 1.6605402E-27

cutoff_LJ = 12
cutoff_coul = 30

adsorbate_coords = {'carbon dioxide': np.array([[0, 0, 0], [0, 0, 1.16], [0, 0, -1.16]]),
             'nitrogen': np.array([[0, 0, 0], [0, 0, 0.55], [0, 0, -0.55]]),
             'propyl bromide': np.array([[-2.04435097,  0.10812424, -0.28972781],
                                        [-0.65791197,  0.27943784,  0.35837579],
                                        [ 0.49454183, -0.30116032, -0.48207421],
                                        [ 2.20772113, -0.08640176,  0.41342624]])}
adsorbate_atoms = {'carbon dioxide': ['C', 'O', 'O'],
             'nitrogen': ['COM', 'N', 'N'],
             'propyl bromide': ['CH3', 'CH2', 'CH2', 'Br']}
adsorbate_charges = {'carbon dioxide': np.array([0.7, -0.35, -0.35]),
               'nitrogen': np.array([0.964, -0.482, -0.482]),
               'propyl bromide': np.array([-0.106, 0.246, 0.085, -0.225])}
length_mol = {'carbon dioxide': 1.16, 'nitrogen': 0.55}

convert_to_short_name = {'carbon dioxide': 'CO2', 'nitrogen': 'N2', 
                         'methane': 'CH4', 'PFBA': 'PFBA'}

# Pore blocking info
pore_blocking_radius = {'CO2': 1.22, 'N2': 1.324, 'CH4': 1.492}
pore_blocking_radius_per_A3 = 100
pore_blocking_folder = '/UFF_rad'

fmt_version_def = FMTVersion.AntiSymWhiteBear

# DFT solver parameter
beta_picard_1_def = 0.01
max_iter_picard_1_def = 50
beta_picard_2_def = 0.1
tol_picard_2_def = 1.0e-5 # default = 1.0e-5
mmax_anderson_mixing_def = 10
tol_anderson_mixing_def = 1.0e-8 # 1e-8 usually

max_energy = 50 # kJ/mol in the external potential

# 
kb = 1.380648*10**-23
inv_4_epsi0 = 1/(4*np.pi*8.8541878128*10**-12)
charge_e_to_c = 1.602176634*10**-19
factor_coulomb = inv_4_epsi0*charge_e_to_c**2/kb/10**-10

angle_step = 20