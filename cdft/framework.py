import numpy as np
import pandas as pd
from pymatgen.io.cif import CifParser

from feos.si import *

from .variables import cifparser_occupancy_tolerance_def, mass_atom, \
                      cutoff_LJ, cutoff_coul


class Framework:

    def __init__(self, cif_path, framework_ff, n_grid_def, n_grid):  

        # Define all geometric information of the framework
        self.cif_path = cif_path
        self.cif_parser = CifParser(
            self.cif_path, occupancy_tolerance=cifparser_occupancy_tolerance_def)
        self.structure_adsorbent = \
            self.cif_parser.get_structures(primitive=False)[0]
        self.volume_cell = self.structure_adsorbent.volume
        self.get_lattice_and_coordinates()
        self.get_system_size()
        self.angles = [self.alpha_deg*DEGREES, self.beta_deg*DEGREES, 
                self.gamma_deg*DEGREES]
        self.num_atoms = self.coordinates.shape[0]

        try:
            if len(self.cif_parser.as_dict().keys()) != 1:
                print('More than 1 file in cif, cannot read charges')
                stop
            else:
                for i in self.cif_parser.as_dict().keys():
                    self.charges = \
                        self.cif_parser.as_dict()[i]['_atom_site_charge']
                    self.charges = np.array(self.charges, dtype=float)
        except:
            self.charges = np.zeros([self.coordinates.shape[0]]) 

        # Define interaction information of the framework
        # Lennard-Jones interactions
        self.set_solid_gas_interactions(framework_ff)
        # Charges
   

        # Compute grids
        self.n_grid_def = n_grid_def
        self.n_grid = n_grid
        self.get_grid_size()
        self.set_grid()

        # Periodic lattices
        self.rep_lat_LJ, self.rep_lat_inv_LJ, self.periodic_coord_LJ, \
            _, self.periodic_sigma_LJ, \
            self.periodic_epsilon_LJ = \
                self.compute_periodic_info(cutoff_LJ)
        
        self.rep_lat_coul, self.rep_lat_inv_coul, self.periodic_coord_coul, \
            self.periodic_charges_coul, _, _ = \
                self.compute_periodic_info(cutoff_coul)


    def get_lattice_and_coordinates(self):
        # Get the coordinates of each atom
        # dic = self.cif_parser.as_dict()
        # dic = dic[list(dic.keys())[0]]
        self.a = self.structure_adsorbent.lattice.a
        self.b = self.structure_adsorbent.lattice.b
        self.c = self.structure_adsorbent.lattice.c
        self.alpha_rad = self.structure_adsorbent.lattice.angles[0]*2*np.pi/360
        self.beta_rad = self.structure_adsorbent.lattice.angles[1]*2*np.pi/360
        self.gamma_rad = self.structure_adsorbent.lattice.angles[2]*2*np.pi/360
        self.alpha_deg = self.structure_adsorbent.lattice.angles[0]
        self.beta_deg = self.structure_adsorbent.lattice.angles[1]
        self.gamma_deg = self.structure_adsorbent.lattice.angles[2]
        self.lattice = self.get_lattice_RASPA()
        self.coordinates = \
            (self.structure_adsorbent.frac_coords@self.lattice)
        
    def get_lattice_RASPA(self):
        # This function calculates the lattice like RASPA because it is 
        # different from the one in pymatgen
        xi = (np.cos(self.alpha_rad) \
              - np.cos(self.gamma_rad)*np.cos(self.beta_rad))\
              /np.sin(self.gamma_rad)
        lat_test = np.array([
            [self.a, self.b*np.cos(self.gamma_rad), self.c*np.cos(self.beta_rad)],
            [0, self.b*np.sin(self.gamma_rad), self.c*xi],
            [0, 0, self.c*np.sqrt(1 - np.cos(self.beta_rad)**2 - xi**2)]])
        return lat_test.T

    def get_system_size(self):
        # Get the size of the cell.
        self.system_size = [\
            l*ANGSTROM for l in self.structure_adsorbent.lattice.lengths]
        

    def set_solid_gas_interactions(self, framework_ff):

        # Read force field file
        # print(forcefield)
        forcefield = pd.read_csv(framework_ff, sep=r'\s+',  
                                  names=["type","sigma","epsilon","mass"])

        # Initialize sigma, epsilon and mass.
        self.sigma = np.zeros(self.structure_adsorbent.num_sites)
        self.epsilon = np.zeros(self.structure_adsorbent.num_sites)
        self.mass = 0.0

        # For each atom, define the Lennard Jones parameters (sigma and epsilon)
        # in function of the atom type. Compute the total mass of the solid.
        for i, site in enumerate(self.structure_adsorbent):
            if ':' not in site.species_string:
                atom_idx = forcefield["type"]==site.species_string
                self.sigma[i] = float(forcefield["sigma"][atom_idx].iloc[0])
                self.epsilon[i] = float(forcefield["epsilon"][atom_idx].iloc[0])
                self.mass += float(forcefield["mass"][atom_idx].iloc[0])*mass_atom
            else:
                species_string = site.species_string.split(':')[0]
                partial_occupancy = float(site.species_string.split(':')[1])
                atom_idx = forcefield["type"]==species_string
                self.sigma[i] = float(forcefield["sigma"][atom_idx].iloc[0])
                self.epsilon[i] = float(forcefield["epsilon"][atom_idx].iloc[0])*partial_occupancy
                self.mass += float(forcefield["mass"][atom_idx].iloc[0])*mass_atom*partial_occupancy
                self.charges[i] *= partial_occupancy
                print('This cif file has partial occupancy and this is not well considered in\
                      this framework yet.')
    
    def get_grid_size(self):
        if self.n_grid_def == 'density':
            self.grid_size = [int(l/ANGSTROM*self.n_grid)\
                            for l in self.system_size]
        elif self.n_grid_def == 'value':
            self.grid_size = [self.n_grid, self.n_grid, self.n_grid]
        
    def set_grid(self):
        self.grid = np.zeros([self.grid_size[0], self.grid_size[1], 
                              self.grid_size[2], 3])
        for i in range(self.grid_size[0]):
            for j in range(self.grid_size[1]):
                for k in range(self.grid_size[2]):
                    self.grid[i, j, k] = i/self.grid_size[0]*self.lattice[0] \
                                         + self.lattice[0]/(2*self.grid_size[0]) \
                                         + j/self.grid_size[1]*self.lattice[1] \
                                         + self.lattice[1]/(2*self.grid_size[1]) \
                                         + k/self.grid_size[2]*self.lattice[2] \
                                         + self.lattice[2]/(2*self.grid_size[2])
                    
    def compute_periodic_info(self, cutoff):
        # Given a cutoff, this function repeats the unit cell the number of times
        # that is necessary to have the cutoff more than twice smaller than 
        # the distance between the two faces in one direction. This function
        # repeats the coordinates but also the charges, sigma, and epsilon to
        # facilitate calculations of energies later.
        rep, rep_lat, rep_lat_inv = self.compute_num_images(cutoff)
        rep_num_atoms = self.num_atoms*rep[0]*rep[1]*rep[2]

        periodic_coordinates = np.zeros([rep_num_atoms, 3])
        periodic_charges = np.zeros([rep_num_atoms])
        periodic_sigma = np.zeros([rep_num_atoms])
        periodic_epsilon = np.zeros([rep_num_atoms])
        count = 0
        for i in range(rep[0]):
            for j in range(rep[1]):
                for k in range(rep[2]):
                    for a in range(self.num_atoms):
                        image = i*self.lattice[0] + j*self.lattice[1] \
                                + k*self.lattice[2]
                        periodic_coordinates[count] = \
                            self.coordinates[a] + image
                        periodic_charges[count] = self.charges[a]
                        periodic_sigma[count] = self.sigma[a]
                        periodic_epsilon[count] = self.epsilon[a]
                        count += 1
        return rep_lat, rep_lat_inv, periodic_coordinates, \
               periodic_charges, periodic_sigma, periodic_epsilon
    
    def compute_num_images(self, cutoff):
        # This function computes the number of images in each direction that is
        # necessary to have the cutoff smaller than the half of distances 
        # between faces.
        ws = np.zeros([self.lattice.shape[0]])
        rep = np.zeros([self.lattice.shape[0]], dtype=int)
        rep_lat = np.zeros(self.lattice.shape)
        for i in range(self.lattice.shape[0]):
            cross_prod = np.cross(self.lattice[(i + 1) % 3], 
                                  self.lattice[(i + 2) % 3])
            ws[i] = np.linalg.norm(
                np.dot(self.lattice[i], cross_prod))/np.linalg.norm(cross_prod)
            rep[i] = np.ceil(2*cutoff/ws[i])
            rep_lat[i] = rep[i] * self.lattice[i]
        rep_lat_inv = np.linalg.inv(rep_lat)
        return rep, rep_lat, rep_lat_inv