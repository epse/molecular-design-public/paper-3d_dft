This folder is organized in several parts:
- The folder Data/ contains the information about the MOF (cif files and block spheres), the forcefield parameters and the PC-SAFT parameters.
- The code to run cDFT: You mostly have to modify example.py in a first step.  

To use this code:
1) Create a python environment with Python 3.10
2) Navigate to the root folder of this project
3) run 'pip3 install .'

To see if everything works, run example.py with the following parameters:
```
<<
cif_path = '../../Data/Cleaned_data/cif_files/EPFL/RSM0016.cif'
framework_ff = '../../Data/Forcefield/UFF.dat'
adsorbates_name = ['methane'] # The adsorbate name is the same as in the PC-SAFT parameters file
interaction_type =  ['LJ_only'] # Can only be 'LJ_only', 'LJ_Coulomb_fast_sampling', or 'LJ_Coulomb_full_sampling' 
adsorbates_pcsaft = ['../../Data/PC_SAFT_params/gross2001.json']
adsorbates_external_pot = ['../../Data/Forcefield/TraPPe_CO2.dat'] # Ignore if no Coulomb interactions.
pore_blocking_bool = 0 # Boolean to know if pore blocking is considered
pore_blocking_file = 'Data/MOF/block_spheres/UFF_rad/RSM0016_CH4_1.492_100.block' # If pore_blocking_bool is 0, this is ignored
n_grid_def = 'density' # If 'value', the number given in 'n_grid' is the number of grid points in each direction
                       # If 'density', the value given in 'n_grid' is the number of grid points per Angstrom
n_grid = 2


# To run cDFT
temperature = 298.15 # In K
pressures = np.array([1, 2]) # In bar
frac_adsorbates = np.array([1.0])
>>
```

You should obtain values around [0.09187129, 0.18024226] mol/kg for the loading, and [-16.88622146, -16.9618649]. The calculations should be quick (less than 1 minute).

For N2, do the following changes:
`adsorbates_name = ['nitrogen']`
`pore_blocking_file = 'Data/MOF/block_spheres/UFF_rad/RSM0016_N2_1.324_100.block'`

You should obtain values of [0.05816953, 0.11429975] mol/kg for the loading, and [-14.80288291, -14.82811181]. The calculations should be quick (less than 1 minute).

For CO2, do the following changes:
```
adsorbates_name = ['carbon dioxide']
interaction_type =  ['LJ_Coulomb_fast_sampling']
adsorbates_pcsaft = ['../../Data/PC_SAFT_params/gross2005_fit.json']
pore_blocking_file = 'Data/MOF/block_spheres/UFF_rad/RSM0016_CO2_1.22_100.block'
```


You should obtain values of [0.53556903, 0.9009246] mol/kg for the loading, and [-23.61291155, -24.24975752]. The calculations should be quick (less than 1 minute).

If you have any question or remark, please email me at vdufour@ethz.ch

