#%%
import time

import numpy as np

from cdft import cDFT

# To define cDFT
cif_path = 'Data/MOF/cif_files/RSM0016.cif'

framework_ff = 'Data/Forcefield/UFF.dat'
adsorbates_name = ['methane']  # The adsorbate name is the same as in the PC-SAFT parameters file
interaction_type =  ['LJ_only']  # Can only be 'LJ_only', 'LJ_Coulomb_fast_sampling', or 'LJ_Coulomb_full_sampling'
adsorbates_pcsaft = ['Data/PC_SAFT_params/gross2001.json']
adsorbates_external_pot = ['Data/Forcefield/TraPPe_CO2.dat']  # Ignored for those with 'LJ_only'
pore_blocking_bool = 1 # Boolean to know if pore blocking is considered
pore_blocking_file = 'Data/MOF/block_spheres/UFF_rad/RSM0016_CH4_1.492_100.block' # If pore_blocking_bool is 0, this is ignored
n_grid_def = 'density' # If 'value', the number given in 'n_grid' is the number of grid points in each direction
                     # If 'density', the value given in 'n_grid' is the number of grid points per Angstrom
n_grid = 2

# To run cDFT
temperature = 298.15  # in K
pressures = np.array([1, 2])  # in bar
frac_adsorbates = np.array([1.0])

if __name__ == "__main__":
    t0 = time.time()
    cDFT_setup = cDFT(cif_path, framework_ff, adsorbates_name,
                        interaction_type, adsorbates_pcsaft,
                        adsorbates_external_pot, pore_blocking_bool, 
                        pore_blocking_file, n_grid_def, n_grid, temperature)
    cDFT_setup.compute_loading_and_enthalpy(temperature, pressures, frac_adsorbates)
    print(f"loading absolute (mol/kg):\n{cDFT_setup.loading_absolute}\n")
    print(f"head of adsorption (kJ/mol):\n{cDFT_setup.enthalpy_of_adsorption}\n")
    print(f"runtime (sec): {time.time() - t0:.2f}")
